variable "app_name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "public_subnet_id" {
  type = string
}

variable "public_subnet_id_2" {
  type = string
}

variable "host" {
  default = "85.118.181.8"
}

variable "port" {
  default = "80"
}

variable "instance_ami" {
  default = "ami-0d527b8c289b4af7f"
}