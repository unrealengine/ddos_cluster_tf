output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.vpc.id
}

output "public_subnet_id" {
  description = "ID of public subnet"
  value       = aws_subnet.public_subnet.id
}

output "public_subnet_id_2" {
  description = "ID of public subnet"
  value       = aws_subnet.public_subnet_2.id
}
