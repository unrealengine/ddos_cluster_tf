provider "aws" {
  region = "eu-central-1"
}

module "vpc" {
  source = "../modules/VPC/"
}

module "ec2" {
  source = "../modules/EC2"
  app_name = "drupal"
  vpc_id = module.vpc.vpc_id
  public_subnet_id = module.vpc.public_subnet_id
  public_subnet_id_2 = module.vpc.public_subnet_id_2
}